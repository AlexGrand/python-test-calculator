FROM python:3.8.6

COPY . /python-test-calculator

WORKDIR /python-test-calculator

RUN pip install --no-cache-dir -r requirements.txt

RUN ["pytest", "-v", "--junitxml=reports/result.xml"]

# CMD tail -f /dev/null keeps the container running even after test completion so that we can 
# copy the result.xml file from the test container to our workspace in 
# Jenkins for it to publish the test report.
CMD tail -f /dev/null
