# python-test-calculator

Simple project to test Pytest + Jenkins in a Docker

**Setup**
1. Build Jenkins image that holds installed docker inside:
```sh
docker build -t jenkins-with-docker -f JenkinsDockerfile .
```
2. Run container with a docker socket:
```sh
docker run -d -p 8080:8080 --name jenkins-with-docker -v /var/run/docker.sock:/var/run/docker.sock jenkins-docker
```
3. Go to `localhost:8080`
4. Enter Jenkins and create your project. Freestyle project for testing purposes
5. Add git integration and creds
6. Go to `build step -> Execute shell` and add sh snippet:
```sh
IMAGE_NAME="test-image"
CONTAINER_NAME="test-container"
echo "Check current working directory"
pwd
echo "Build docker image and run container"
docker build -t $IMAGE_NAME .
docker run -d --name $CONTAINER_NAME $IMAGE_NAME
echo "Copy result.xml into Jenkins container"
rm -rf reports; mkdir reports
docker cp $CONTAINER_NAME:/python-test-calculator/reports/result.xml reports/
echo "Cleanup"
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker rmi $IMAGE_NAME
```
7. In Post-Build Actions specify JUnit reports path: `reports\result.xml`
8. Run Jenkins Job
